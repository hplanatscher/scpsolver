/* glpk_jni.c (Java Native Interface for GLPK 4.8) */

/* ---------------------------------------------------------------------
-- Copyright (C) 2003 Andrew Makhorin <mao@mai2.rcnet.ru>, Department
-- for Applied Informatics, Moscow Aviation Institute, Moscow, Russia.
-- All rights reserved.
--
-- Author: Yuri Victorovich, Software Engineer, yuri@gjt.org.
--				-- GLPK 4.2 JNI
-- Author: Bjoern Frank, byuman /at] gmx {dot\ net
--				-- GLPK 4.8 JNI
-- This file is a part of GLPK (GNU Linear Programming Kit).
--  
-- GLPK is free software; you can redistribute it and/or modify it
-- under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2, or (at your option)
-- any later version.
--  
-- GLPK is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
-- or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
-- License for more details.
--  
-- You should have received a copy of the GNU General Public License
-- along with GLPK; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
-- 02111-1307, USA.
------------------------------------------------------------------------*/


#include <jni.h>
#include "lpx.h"
#include <stdlib.h>


#include "org_gnu_glpk_GlpkSolver.h"

/* Define undocumented GLPK functions */
void _glp_lib_print_hook(int (*func)(void *info, char *buf), void *info);
void _glp_lib_fault_hook(int (*func)(void *info, char *buf), void *info);

/* ***** preparatory functions to communicate between C and Java ***** */

/* write C lp pointer value to Java GlpkSolver lp (int) field
   to indicate status of lp object */
static void put_lpx(JNIEnv *env, jobject obj, LPX *lp)
{
	jclass cls = (*env)->GetObjectClass(env, obj);
	jfieldID fid = (*env)->GetFieldID(env, cls, "lp", "J");
	(*env)->SetLongField(env, obj, fid, (jlong) lp);
}


/* convert Java GlpkSolver object into C pointer to object
   if such an object does not exist, create one */
static LPX * get_lpx(JNIEnv *env, jobject obj)
{
	LPX *lp;

	jclass cls = (*env)->GetObjectClass(env, obj);
	jfieldID fid = (*env)->GetFieldID(env, cls, "lp", "J");
	
	lp = (LPX*)(*env)->GetLongField(env, obj, fid);

	if (lp != 0) return (lp);
	lp = _glp_lpx_create_prob();
	put_lpx(env, obj, lp);
	return (lp);
}


/* destructor deletes C object and indicates deletion
   by setting GlpkSolver member field int lp = 0 */
JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_finalize(JNIEnv *env, jobject obj)
{
	LPX *lp = get_lpx(env, obj);
	if (lp != 0)
	{
		_glp_lpx_delete_prob(lp);
		put_lpx(env, obj, 0);
	}
}


/* function deletes C object and indicates deletion
   by setting GlpkSolver member field int lp = 0 */
JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_deleteProb(JNIEnv *env, jobject obj)
{
	LPX *lp = get_lpx(env, obj);
	if (lp != 0)
	{
		_glp_lpx_delete_prob(lp);
		put_lpx(env, obj, 0);
	}
}


/* ***** callback for handling printing and fault messages ***** */

/* needed for _hook_fn functions */
typedef struct
{
    JNIEnv  *env;
    jobject obj;
} info_t;


/* Install a callback for handling printing and fault messages */
int _hook_fn(info_t *info, char *msg, int bFault)
{
	int debug=0;
	jclass cls; /* added by Gottfried */
	jstring	callbackArg; /* added by Gottfried */
    jmethodID 	mID; /* added by Gottfried */

    jobject obj=info->obj;
    JNIEnv  *env=info->env;

	if (debug) printf("HERE0: %s\n",msg);

	//jclass 
	cls=(*env)->FindClass(env,"org/gnu/glpk/GlpkSolver");

    if (cls == NULL)
		return 0;

	if (debug) printf("HERE1\n");

	//jstring	
	callbackArg=(*env)->NewStringUTF(env, msg);

	if (debug) printf("HERE2\n");

    //jmethodID 	mID;

    if (bFault)
        mID = (*env)->GetMethodID(env,cls,"faultHook","(Ljava/lang/String;)V");
	else
        mID = (*env)->GetMethodID(env,cls,"printHook","(Ljava/lang/String;)V");

    if (mID == NULL)
        return 0;

    if (debug) printf("HERE3\n");

    (*env)->CallVoidMethod(env, obj, mID, callbackArg);

	if (debug) printf("HERE4\n");

	return 1;
}


/* callback for handling printing messages */
int _fault_hook_fn(void *info, char *msg)
{
	return _hook_fn((info_t*)info,msg,1);
}


/* Install a callback for handling fault messages */
int _print_hook_fn(void *info, char *msg)
{
    return _hook_fn((info_t*)info,msg,0);
}


/* belongs to hook_fn functions */
JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_enablePrints(JNIEnv *env, jobject obj,
										  jboolean enable)
{
    info_t  *info=malloc(sizeof(info_t));

    info->env = env;
    info->obj = obj;

    //lib_set_fault_hook((void*)info, enable ? NULL : &_fault_hook_fn);
    //lib_set_print_hook((void*)info, enable ? NULL : &_print_hook_fn);
}





/* ***** GLPK 4.8 functions ***** */






/* Problem creating and modifying routines (manual p. 17) */

JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setProbName(JNIEnv *env, jobject obj,
	jstring name)
{
	LPX *lp = get_lpx(env, obj);
	char * chr = (char *)(*env)->GetStringUTFChars(env, name, 0);
	_glp_lpx_set_prob_name(lp, chr);
	(*env)->ReleaseStringUTFChars(env, name, chr);
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setObjName(JNIEnv *env, jobject obj,
	jstring name)
{
	LPX *lp = get_lpx(env, obj);
	char * chr = (char *)(*env)->GetStringUTFChars(env, name, 0);
	_glp_lpx_set_obj_name(lp, chr);
	(*env)->ReleaseStringUTFChars(env, name, chr);
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setObjDir(JNIEnv *env, jobject obj, jint dir)
{
	_glp_lpx_set_obj_dir(get_lpx(env, obj), (int)dir);
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_addRows(JNIEnv *env, jobject obj, jint nrs)
{
	_glp_lpx_add_rows(get_lpx(env, obj), (int)nrs);
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_addCols(JNIEnv *env, jobject obj, jint ncs)
{
	_glp_lpx_add_cols(get_lpx(env, obj), (int)ncs);
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setRowName(JNIEnv *env, jobject obj,
	jint i, jstring name)
{
	LPX *lp = get_lpx(env, obj);
	char * chr = (char *)(*env)->GetStringUTFChars(env, name, 0);
	_glp_lpx_set_row_name(lp, (int) i, chr);
	(*env)->ReleaseStringUTFChars(env, name, chr);
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setColName(JNIEnv *env, jobject obj,
	jint j, jstring name)
{
	LPX *lp = get_lpx(env, obj);
	char * chr = (char *)(*env)->GetStringUTFChars(env, name, 0);
	_glp_lpx_set_col_name(lp, (int)j, chr);
	(*env)->ReleaseStringUTFChars(env, name, chr);
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setRowBnds(JNIEnv *env, jobject obj, jint i,
	jint type, jdouble lb, jdouble ub)
{
	LPX *lp = get_lpx(env, obj);
	_glp_lpx_set_row_bnds(lp, (int)i, (int)type, (double)lb, (double)ub);
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setColBnds(JNIEnv *env, jobject obj, jint j,
	jint type, jdouble lb, jdouble ub)
{
	LPX *lp = get_lpx(env, obj);
	_glp_lpx_set_col_bnds(lp, (int)j, (int)type, (double)lb, (double)ub);
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setObjCoef(JNIEnv *env, jobject obj, 
										jint j, jdouble coef)
{
	LPX *lp = get_lpx(env, obj);
	_glp_lpx_set_obj_coef(lp, (int)j, (double)coef);

}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setMatRow(JNIEnv *env, jobject obj,
	jint i, jint len, jintArray ind, jdoubleArray val)
{
	LPX *lp = get_lpx(env, obj);

	jint *indArray   = (*env)->GetIntArrayElements(env, ind, 0);
	jdouble *valArray = (*env)->GetDoubleArrayElements(env, val, 0);

	_glp_lpx_set_mat_row(lp, (int)i, (int) len, (int*)indArray, (double*)valArray);

	(*env)->ReleaseIntArrayElements(env, ind, indArray, 0);
	(*env)->ReleaseDoubleArrayElements(env, val, valArray, 0);
}



JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setMatCol(JNIEnv *env, jobject obj,
	jint j, jint len, jintArray ind, jdoubleArray val)
{
	LPX *lp = get_lpx(env, obj);

	jint *indArray   = (*env)->GetIntArrayElements(env, ind, 0);
	jdouble *valArray = (*env)->GetDoubleArrayElements(env, val, 0);

	_glp_lpx_set_mat_col(lp, (int)j, (int) len, (int*)indArray, (double*)valArray);

	(*env)->ReleaseIntArrayElements(env, ind, indArray, 0);
	(*env)->ReleaseDoubleArrayElements(env, val, valArray, 0);
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_loadMatrix(JNIEnv *env, jobject obj,
	jint ne, jintArray ia, jintArray ja, jdoubleArray ar)
{
	LPX *lp = get_lpx(env, obj);

	jint *iaArray   = (*env)->GetIntArrayElements(env, ia, 0);
	jint *jaArray   = (*env)->GetIntArrayElements(env, ja, 0);
	jdouble *arArray = (*env)->GetDoubleArrayElements(env, ar, 0);

	_glp_lpx_load_matrix(lp, (int)ne, (int*)iaArray, (int*)jaArray, (double*)arArray);

	(*env)->ReleaseIntArrayElements(env, ia, iaArray, 0);
	(*env)->ReleaseIntArrayElements(env, ja, jaArray, 0);
	(*env)->ReleaseDoubleArrayElements(env, ar, arArray, 0);
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_delRows(JNIEnv *env, jobject obj,
	jint nrs, jintArray num)
{
	LPX *lp = get_lpx(env, obj);
	jint *numArray   = (*env)->GetIntArrayElements(env, num, 0);

	_glp_lpx_del_rows(lp, (int)nrs, (int*)numArray);

	(*env)->ReleaseIntArrayElements(env, num, numArray, 0);
}

JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_delCols(JNIEnv *env, jobject obj,
	jint ncs, jintArray num)
{
	LPX *lp = get_lpx(env, obj);
	jint *numArray   = (*env)->GetIntArrayElements(env, num, 0);

	_glp_lpx_del_cols(lp, (int)ncs, (int*)numArray);

	(*env)->ReleaseIntArrayElements(env, num, numArray, 0);
}






/* Problem retrieving routines (manual p. 23) */

JNIEXPORT jstring JNICALL
Java_org_gnu_glpk_GlpkSolver_getProbName(JNIEnv *env, jobject obj)
{
	return (*env)->NewStringUTF(env, _glp_lpx_get_prob_name(get_lpx(env, obj)));
}


JNIEXPORT jstring JNICALL
Java_org_gnu_glpk_GlpkSolver_getObjName(JNIEnv *env, jobject obj)
{
	return (*env)->NewStringUTF(env, _glp_lpx_get_obj_name(get_lpx(env, obj)));
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getObjDir(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_get_obj_dir(get_lpx(env, obj));
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getNumRows(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_get_num_rows(get_lpx(env, obj));
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getNumCols(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_get_num_cols(get_lpx(env, obj));
}


JNIEXPORT jstring JNICALL
Java_org_gnu_glpk_GlpkSolver_getRowName(JNIEnv *env, jobject obj, jint i)
{
	return (*env)->NewStringUTF(env, _glp_lpx_get_row_name(get_lpx(env, obj), (int)i));
}

JNIEXPORT jstring JNICALL
Java_org_gnu_glpk_GlpkSolver_getColName(JNIEnv *env, jobject obj, jint j)
{
	return (*env)->NewStringUTF(env, _glp_lpx_get_col_name(get_lpx(env, obj), (int)j));
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getRowType(JNIEnv *env, jobject obj, jint i)
{
	return (jint)_glp_lpx_get_row_type(get_lpx(env, obj), (int)i);
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_getRowLb(JNIEnv *env, jobject obj, jint i)
{
	return (jdouble)_glp_lpx_get_row_lb(get_lpx(env, obj), (int)i);
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_getRowUb(JNIEnv *env, jobject obj, jint i)
{
	return (jdouble)_glp_lpx_get_row_ub(get_lpx(env, obj), (int)i);
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getColType(JNIEnv *env, jobject obj, jint j)
{
	return (jint)_glp_lpx_get_col_type(get_lpx(env, obj), (int)j);
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_getColLb(JNIEnv *env, jobject obj, jint j)
{
	return (jdouble)_glp_lpx_get_col_lb(get_lpx(env, obj), (int)j);
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_getColUb(JNIEnv *env, jobject obj, jint j)
{
	return (jdouble)_glp_lpx_get_col_ub(get_lpx(env, obj), (int)j);
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_getObjCoef(JNIEnv *env, jobject obj, jint j)
{
	return (jdouble)_glp_lpx_get_obj_coef(get_lpx(env, obj), (int)j);
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getNumNz(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_get_num_nz(get_lpx(env, obj));
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getMatRow(JNIEnv *env, jobject obj, jint i,
	jintArray ind, jdoubleArray val)
{
	int ret;
	LPX *lp = get_lpx(env, obj);
	jint *indArray = (*env)->GetIntArrayElements(env, ind, 0);
	jdouble *valArray = (*env)->GetDoubleArrayElements(env, val, 0);

	ret = _glp_lpx_get_mat_row(lp, (int)i, (int*)indArray, (double*)valArray);

	(*env)->ReleaseIntArrayElements(env, ind, indArray, 0);
	(*env)->ReleaseDoubleArrayElements(env, val, valArray, 0);

	return ((jint)ret);
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getMatCol(JNIEnv *env, jobject obj, jint j,
	jintArray ind, jdoubleArray val)
{
	int ret;
	LPX *lp = get_lpx(env, obj);
	jint *indArray = (*env)->GetIntArrayElements(env, ind, 0);
	jdouble *valArray = (*env)->GetDoubleArrayElements(env, val, 0);

	ret = _glp_lpx_get_mat_col(lp, (int)j, (int*)indArray, (double*)valArray);

	(*env)->ReleaseIntArrayElements(env, ind, indArray, 0);
	(*env)->ReleaseDoubleArrayElements(env, val, valArray, 0);

	return ((jint)ret);
}






/* Problem scaling routines (manual p. 28) */

JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_scaleProb(JNIEnv *env, jobject obj)
{
	_glp_lpx_scale_prob(get_lpx(env, obj));
}

JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_unscaleProb(JNIEnv *env, jobject obj)
{
	_glp_lpx_unscale_prob(get_lpx(env, obj));
}







/* LP Basis constructing routines (manual p. 29) */

JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_stdBasis(JNIEnv *env, jobject obj)
{
	_glp_lpx_std_basis(get_lpx(env, obj));
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_advBasis(JNIEnv *env, jobject obj)
{
	_glp_lpx_adv_basis(get_lpx(env, obj));
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setRowStat(JNIEnv *env, jobject obj,
	jint i, jint stat)
{
	_glp_lpx_set_row_stat(get_lpx(env, obj), (int)i, (int)stat);
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setColStat(JNIEnv *env, jobject obj,
	jint j, jint stat)
{
	_glp_lpx_set_col_stat(get_lpx(env, obj), (int)j, (int)stat);
}




/* Simplex method routine (manual p. 31) */

JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_simplex(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_simplex(get_lpx(env, obj));
}





/* Basic solution retrieving routines (manual p. 33) */


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getStatus(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_get_status(get_lpx(env, obj));
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getPrimStat(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_get_prim_stat(get_lpx(env, obj));
}

JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getDualStat(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_get_dual_stat(get_lpx(env, obj));
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_getObjVal(JNIEnv *env, jobject obj)
{
	return (jdouble)_glp_lpx_get_obj_val(get_lpx(env, obj));
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getRowStat(JNIEnv *env, jobject obj,
										jint i)
{
	return (jint)_glp_lpx_get_row_stat(get_lpx(env, obj), (int)i);
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_getRowPrim(JNIEnv *env, jobject obj,
										jint i)
{
	return (jdouble)_glp_lpx_get_row_prim(get_lpx(env, obj), (int)i);
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_getRowDual(JNIEnv *env, jobject obj,
										jint i)
{
	return (jdouble)_glp_lpx_get_row_dual(get_lpx(env, obj), (int)i);
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getColStat(JNIEnv *env, jobject obj,
										jint j)
{
	return (jint)_glp_lpx_get_col_stat(get_lpx(env, obj), (int)j);
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_getColPrim(JNIEnv *env, jobject obj,
										jint j)
{
	return (jdouble)_glp_lpx_get_col_prim(get_lpx(env, obj), (int)j);
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_getColDual(JNIEnv *env, jobject obj,
										jint j)
{
	return (jdouble)_glp_lpx_get_col_dual(get_lpx(env, obj), (int)j);
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getRayInfo(JNIEnv *env, jobject obj)										
{
	return (jint)_glp_lpx_get_ray_info(get_lpx(env, obj));
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_checkKkt(JNIEnv *env, jobject obj, jint scaled,
  jobject jkkt)
{
  LPX *lp = get_lpx(env, obj);
  LPXKKT kkt;
  jclass cls = (*env)->GetObjectClass(env, jkkt);
  jfieldID fid_pe_ae_max, fid_pe_ae_row, fid_pe_re_max, fid_pe_re_row,
           fid_pe_quality, fid_pb_ae_max, fid_pb_ae_ind, fid_pb_re_max,
           fid_pb_re_ind, fid_pb_quality, fid_de_ae_max, fid_de_ae_col,
           fid_de_re_max, fid_de_re_col, fid_de_quality, fid_db_ae_max,
           fid_db_ae_ind, fid_db_re_max, fid_db_re_ind, fid_db_quality,
           fid_cs_ae_max, fid_cs_ae_ind, fid_cs_re_max, fid_cs_re_ind,
           fid_cs_quality;
  if ((fid_pe_ae_max = (*env)->GetFieldID(env, cls, "pe_ae_max", "D")) == 0 ||
      (fid_pe_ae_row = (*env)->GetFieldID(env, cls, "pe_ae_row", "I")) == 0 ||
      (fid_pe_re_max = (*env)->GetFieldID(env, cls, "pe_re_max", "D")) == 0 ||
      (fid_pe_re_row = (*env)->GetFieldID(env, cls, "pe_re_row", "I")) == 0 ||
      (fid_pe_quality= (*env)->GetFieldID(env, cls, "pe_quality","I")) == 0 ||
      (fid_pb_ae_max = (*env)->GetFieldID(env, cls, "pb_ae_max", "D")) == 0 ||
      (fid_pb_ae_ind = (*env)->GetFieldID(env, cls, "pb_ae_ind", "I")) == 0 ||
      (fid_pb_re_max = (*env)->GetFieldID(env, cls, "pb_re_max", "D")) == 0 ||
      (fid_pb_re_ind = (*env)->GetFieldID(env, cls, "pb_re_ind", "I")) == 0 ||
      (fid_pb_quality= (*env)->GetFieldID(env, cls, "pb_quality","I")) == 0 ||
      (fid_de_ae_max = (*env)->GetFieldID(env, cls, "de_ae_max", "D")) == 0 ||
      (fid_de_ae_col = (*env)->GetFieldID(env, cls, "de_ae_col", "I")) == 0 ||
      (fid_de_re_max = (*env)->GetFieldID(env, cls, "de_re_max", "D")) == 0 ||
      (fid_de_re_col = (*env)->GetFieldID(env, cls, "de_re_col", "I")) == 0 ||
      (fid_de_quality= (*env)->GetFieldID(env, cls, "de_quality","I")) == 0 ||
      (fid_db_ae_max = (*env)->GetFieldID(env, cls, "db_ae_max", "D")) == 0 ||
      (fid_db_ae_ind = (*env)->GetFieldID(env, cls, "db_ae_ind", "I")) == 0 ||
      (fid_db_re_max = (*env)->GetFieldID(env, cls, "db_re_max", "D")) == 0 ||
      (fid_db_re_ind = (*env)->GetFieldID(env, cls, "db_re_ind", "I")) == 0 ||
      (fid_db_quality= (*env)->GetFieldID(env, cls, "db_quality","I")) == 0 ||
      (fid_cs_ae_max = (*env)->GetFieldID(env, cls, "cs_ae_max", "D")) == 0 ||
      (fid_cs_ae_ind = (*env)->GetFieldID(env, cls, "cs_ae_ind", "I")) == 0 ||
      (fid_cs_re_max = (*env)->GetFieldID(env, cls, "cs_re_max", "D")) == 0 ||
      (fid_cs_re_ind = (*env)->GetFieldID(env, cls, "cs_re_ind", "I")) == 0 ||
      (fid_cs_quality= (*env)->GetFieldID(env, cls, "cs_quality","I")) == 0)
    return;

  _glp_lpx_check_kkt(lp, (int)scaled, &kkt);

  (*env)->SetDoubleField(env, jkkt, fid_pe_ae_max,  (jdouble)kkt.pe_ae_max);
  (*env)->SetIntField   (env, jkkt, fid_pe_ae_row,  (jint)   kkt.pe_ae_row);
  (*env)->SetDoubleField(env, jkkt, fid_pe_re_max,  (jdouble)kkt.pe_re_max);
  (*env)->SetIntField   (env, jkkt, fid_pe_re_row,  (jint)   kkt.pe_re_row);
  (*env)->SetIntField   (env, jkkt, fid_pe_quality, (jint)   kkt.pe_quality);
  (*env)->SetDoubleField(env, jkkt, fid_pb_ae_max,  (jdouble)kkt.pb_ae_max);
  (*env)->SetIntField   (env, jkkt, fid_pb_ae_ind,  (jint)   kkt.pb_ae_ind);
  (*env)->SetDoubleField(env, jkkt, fid_pb_re_max,  (jdouble)kkt.pb_re_max);
  (*env)->SetIntField   (env, jkkt, fid_pb_re_ind,  (jint)   kkt.pb_re_ind);
  (*env)->SetIntField   (env, jkkt, fid_pb_quality, (jint)   kkt.pb_quality);
  (*env)->SetDoubleField(env, jkkt, fid_de_ae_max,  (jdouble)kkt.de_ae_max);
  (*env)->SetIntField   (env, jkkt, fid_de_ae_col,  (jint)   kkt.de_ae_col);
  (*env)->SetDoubleField(env, jkkt, fid_de_re_max,  (jdouble)kkt.de_re_max);
  (*env)->SetIntField   (env, jkkt, fid_de_re_col,  (jint)   kkt.de_re_col);
  (*env)->SetIntField   (env, jkkt, fid_de_quality, (jint)   kkt.de_quality);
  (*env)->SetDoubleField(env, jkkt, fid_db_ae_max,  (jdouble)kkt.db_ae_max);
  (*env)->SetIntField   (env, jkkt, fid_db_ae_ind,  (jint)   kkt.db_ae_ind);
  (*env)->SetDoubleField(env, jkkt, fid_db_re_max,  (jdouble)kkt.db_re_max);
  (*env)->SetIntField   (env, jkkt, fid_db_re_ind,  (jint)   kkt.db_re_ind);
  (*env)->SetIntField   (env, jkkt, fid_db_quality, (jint)   kkt.db_quality);
  (*env)->SetDoubleField(env, jkkt, fid_cs_ae_max,  (jdouble)kkt.cs_ae_max);
  (*env)->SetIntField   (env, jkkt, fid_cs_ae_ind,  (jint)   kkt.cs_ae_ind);
  (*env)->SetDoubleField(env, jkkt, fid_cs_re_max,  (jdouble)kkt.cs_re_max);
  (*env)->SetIntField   (env, jkkt, fid_cs_re_ind,  (jint)   kkt.cs_re_ind);
  (*env)->SetIntField   (env, jkkt, fid_cs_quality, (jint)   kkt.cs_quality);
}






/* LP basis and simplex table routines (manual p. 40) */



JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_warmUp(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_warm_up(get_lpx(env, obj));
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_evalTabRow(JNIEnv *env, jobject obj,
	jint k, jintArray ind, jdoubleArray val)
{
	int ret;
	LPX *lp = get_lpx(env, obj);
	jint *indArray = (*env)->GetIntArrayElements(env, ind, 0);
	jdouble *valArray = (*env)->GetDoubleArrayElements(env, val, 0);

	ret = _glp_lpx_eval_tab_row(lp, (int)k, (int*)indArray, (double*)valArray);

	(*env)->ReleaseIntArrayElements(env, ind, indArray, 0);
	(*env)->ReleaseDoubleArrayElements(env, val, valArray, 0);

	return (ret);
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_evalTabCol(JNIEnv *env, jobject obj,
	jint k, jintArray ind, jdoubleArray val)
{
	int ret;
	LPX *lp = get_lpx(env, obj);
	jint *indArray = (*env)->GetIntArrayElements(env, ind, 0);
	jdouble *valArray = (*env)->GetDoubleArrayElements(env, val, 0);

	ret = _glp_lpx_eval_tab_col(lp, (int)k, (int*)indArray, (double*)valArray);

	(*env)->ReleaseIntArrayElements(env, ind, indArray, 0);
	(*env)->ReleaseDoubleArrayElements(env, val, valArray, 0);

	return (ret);
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_transformRow(JNIEnv *env, jobject obj,
	jint len, jintArray ind, jdoubleArray val)
{
	int ret;
	LPX *lp = get_lpx(env, obj);
	jint *indArray = (*env)->GetIntArrayElements(env, ind, 0);
	jdouble *valArray = (*env)->GetDoubleArrayElements(env, val, 0);

	ret = _glp_lpx_transform_row(lp, (int)len, (int*)indArray, (double*)valArray);

	(*env)->ReleaseIntArrayElements(env, ind, indArray, 0);
	(*env)->ReleaseDoubleArrayElements(env, val, valArray, 0);

	return (ret);
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_transformCol(JNIEnv *env, jobject obj,
	jint len, jintArray ind, jdoubleArray val)
{
	int ret;
	LPX *lp = get_lpx(env, obj);
	jint *indArray = (*env)->GetIntArrayElements(env, ind, 0);
	jdouble *valArray = (*env)->GetDoubleArrayElements(env, val, 0);

	ret = _glp_lpx_transform_col(lp, (int)len, (int*)indArray, (double*)valArray);

	(*env)->ReleaseIntArrayElements(env, ind, indArray, 0);
	(*env)->ReleaseDoubleArrayElements(env, val, valArray, 0);

	return (ret);
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_primRatioTest(JNIEnv *env, jobject obj,
	jint len, jintArray ind, jdoubleArray val, jint how, jdouble tol)
{
	int ret;
	LPX *lp = get_lpx(env, obj);
	jint *indArray = (*env)->GetIntArrayElements(env, ind, 0);
	jdouble *valArray = (*env)->GetDoubleArrayElements(env, val, 0);

	ret = _glp_lpx_prim_ratio_test(lp, (int)len, (int*)indArray, (double*)valArray,
								(int)how, (double)tol);

	(*env)->ReleaseIntArrayElements(env, ind, indArray, 0);
	(*env)->ReleaseDoubleArrayElements(env, val, valArray, 0);

	return (ret);
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_dualRatioTest(JNIEnv *env, jobject obj,
	jint len, jintArray ind, jdoubleArray val, jint how, jdouble tol)
{
	int ret;
	LPX *lp = get_lpx(env, obj);
	jint *indArray = (*env)->GetIntArrayElements(env, ind, 0);
	jdouble *valArray = (*env)->GetDoubleArrayElements(env, val, 0);

	ret = _glp_lpx_dual_ratio_test(lp, (int)len, (int*)indArray, (double*)valArray,
								(int)how, (double)tol);

	(*env)->ReleaseIntArrayElements(env, ind, indArray, 0);
	(*env)->ReleaseDoubleArrayElements(env, val, valArray, 0);

	return (ret);
}






/* Interior point method routines (manual p. 46) */

JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_interior(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_interior(get_lpx(env, obj));
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_iptStatus(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_ipt_status(get_lpx(env, obj));
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_iptObjVal(JNIEnv *env, jobject obj)
{
	return (jdouble)_glp_lpx_ipt_obj_val(get_lpx(env, obj));
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_iptRowPrim(JNIEnv *env, jobject obj,
										jint i)
{
	return (jdouble)_glp_lpx_ipt_row_prim(get_lpx(env, obj), (int)i);
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_iptRowDual(JNIEnv *env, jobject obj,
										jint i)
{
	return (jdouble)_glp_lpx_ipt_row_dual(get_lpx(env, obj), (int)i);
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_iptColPrim(JNIEnv *env, jobject obj,
										jint j)
{
	return (jdouble)_glp_lpx_ipt_col_prim(get_lpx(env, obj), (int)j);
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_iptColDual(JNIEnv *env, jobject obj,
										jint j)
{
	return (jdouble)_glp_lpx_ipt_col_dual(get_lpx(env, obj), (int)j);
}





/* MIP routines (manual p. 49) */


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setClss(JNIEnv *env, jobject obj, jint klass)
{
  _glp_lpx_set_class(get_lpx(env, obj), (int)klass);
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getClss(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_get_class(get_lpx(env, obj));
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setColKind(JNIEnv *env, jobject obj,
	jint j, jint kind)
{
	_glp_lpx_set_col_kind(get_lpx(env, obj), (int)j, (int)kind);
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getColKind(JNIEnv *env, jobject obj, jint j)
{
	return (jint)_glp_lpx_get_col_kind(get_lpx(env, obj), (int)j);
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getNumInt(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_get_num_int(get_lpx(env, obj));
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getNumBin(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_get_num_bin(get_lpx(env, obj));
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_integer(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_integer(get_lpx(env, obj));
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_mipStatus(JNIEnv *env, jobject obj)
{
	return (jint)_glp_lpx_mip_status(get_lpx(env, obj));
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_mipObjVal(JNIEnv *env, jobject obj)
{
	return (jdouble)_glp_lpx_mip_obj_val(get_lpx(env, obj));
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_mipRowVal(JNIEnv *env, jobject obj, jint i)
{
	return (jdouble)_glp_lpx_mip_row_val(get_lpx(env, obj), (int)i);
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_mipColVal(JNIEnv *env, jobject obj, jint j)
{
	return (jdouble)_glp_lpx_mip_col_val(get_lpx(env, obj), (int)j);
}





/* Control parameters and statistics routines (manual p. 53) */

JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_resetParms(JNIEnv *env, jobject obj)
{
	_glp_lpx_reset_parms(get_lpx(env, obj));
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setIntParm(JNIEnv *env, jobject obj,
	jint parm, jint val)
{
	_glp_lpx_set_int_parm(get_lpx(env, obj), (int)parm, (int)val);
}


JNIEXPORT jint JNICALL
Java_org_gnu_glpk_GlpkSolver_getIntParm(JNIEnv *env, jobject obj, 
										jint parm)
{
	return (jint)_glp_lpx_get_int_parm(get_lpx(env, obj), (int)parm);
}


JNIEXPORT void JNICALL
Java_org_gnu_glpk_GlpkSolver_setRealParm(JNIEnv *env, jobject obj,
	jint parm, jdouble val)
{
	_glp_lpx_set_real_parm(get_lpx(env, obj), (int)parm, (double)val);
}


JNIEXPORT jdouble JNICALL
Java_org_gnu_glpk_GlpkSolver_getRealParm(JNIEnv *env, jobject obj,
										 jint parm)
{
	return (jdouble)_glp_lpx_get_real_parm(get_lpx(env, obj), (int)parm);
}







/* Utility routines (manual p. 57) */

/*    *********   does this work?? ***********  */
JNIEXPORT jobject JNICALL
Java_org_gnu_glpk_GlpkSolver_readMps(JNIEnv *env, jclass cls, jstring fname)
{
	jobject ret;
	jmethodID mid;

	const char* chr = (*env)->GetStringUTFChars(env, fname, 0);
	LPX *lp = _glp_lpx_read_mps((char*)chr);
	(*env)->ReleaseStringUTFChars(env, fname, chr);

	if (lp == 0) return (0);

	// mid = constructor id
	mid = (*env)->GetMethodID(env, cls, "<init>", "()V");
	if (mid == 0)
	{
		_glp_lpx_delete_prob(lp);
		return (0);
	}
	// return new object
	ret = (*env)->NewObject(env, cls, mid);
	if ((*env)->ExceptionOccurred(env))
	{
		_glp_lpx_delete_prob(lp);
		return (0);
	}

	// mark object to show that lp object is existant
	put_lpx(env, ret, lp);
	return (ret);
}


JNIEXPORT jboolean JNICALL
Java_org_gnu_glpk_GlpkSolver_writeMps(JNIEnv *env, jobject obj, jstring fname)
{
	jboolean ret;

	const char* chr = (*env)->GetStringUTFChars(env, fname, 0);
	ret = _glp_lpx_write_mps(get_lpx(env, obj), (char*)chr) == 0;
	(*env)->ReleaseStringUTFChars(env, fname, chr);

	return(ret);
}


/*    *********   does this work?? ***********  */
JNIEXPORT jboolean JNICALL
Java_org_gnu_glpk_GlpkSolver_readBas(JNIEnv *env, jobject obj, jstring fname)
{
	jboolean ret;

	const char* chr = (*env)->GetStringUTFChars(env, fname, 0);
	ret = _glp_lpx_read_bas(get_lpx(env, obj), (char*)chr) == 0;
	(*env)->ReleaseStringUTFChars(env, fname, chr);

	return (ret);
}


JNIEXPORT jboolean JNICALL
Java_org_gnu_glpk_GlpkSolver_writeBas(JNIEnv *env, jobject obj, jstring fname)
{
	jboolean ret;

	const char* chr = (*env)->GetStringUTFChars(env, fname, 0);
	ret = _glp_lpx_write_bas(get_lpx(env, obj), (char*)chr) == 0;
	(*env)->ReleaseStringUTFChars(env, fname, chr);

	return (ret);
}



/*    *********   does this work?? ***********  */
JNIEXPORT jobject JNICALL
Java_org_gnu_glpk_GlpkSolver_readFreemps(JNIEnv *env, jclass cls, jstring fname)
{
	jobject ret;
	jmethodID mid;

	const char* chr = (*env)->GetStringUTFChars(env, fname, 0);
	LPX *lp = _glp_lpx_read_freemps((char*)chr);
	(*env)->ReleaseStringUTFChars(env, fname, chr);

	if (lp == 0) return (0);

	// mid = constructor id
	mid = (*env)->GetMethodID(env, cls, "<init>", "()V");
	if (mid == 0)
	{
		_glp_lpx_delete_prob(lp);
		return (0);
	}
	// return new object
	ret = (*env)->NewObject(env, cls, mid);
	if ((*env)->ExceptionOccurred(env))
	{
		_glp_lpx_delete_prob(lp);
		return (0);
	}

	// mark object to show that lp object is existant
	put_lpx(env, ret, lp);
	return (ret);
}


JNIEXPORT jboolean JNICALL
Java_org_gnu_glpk_GlpkSolver_writeFreemps(JNIEnv *env, jobject obj, jstring fname)
{
	jboolean ret;

	const char* chr = (*env)->GetStringUTFChars(env, fname, 0);
	ret = _glp_lpx_write_freemps(get_lpx(env, obj), (char*)chr) == 0;
	(*env)->ReleaseStringUTFChars(env, fname, chr);

	return(ret);
}



/*    *********   does this work?? ***********  */
JNIEXPORT jobject JNICALL
Java_org_gnu_glpk_GlpkSolver_readCpxlp(JNIEnv *env, jclass cls, jstring fname)
{
	jobject ret;
	jmethodID mid;

	const char* chr = (*env)->GetStringUTFChars(env, fname, 0);
	LPX *lp = _glp_lpx_read_cpxlp((char*)chr);
	(*env)->ReleaseStringUTFChars(env, fname, chr);

	if (lp == 0)
	{
		return (0);
	}
	// mid = constructor id
	mid = (*env)->GetMethodID(env, cls, "<init>", "()V");
	if (mid == 0)
	{
		_glp_lpx_delete_prob(lp);
		return (0);
	}
	// return new object
	ret = (*env)->NewObject(env, cls, mid);
	if ((*env)->ExceptionOccurred(env))
	{
		_glp_lpx_delete_prob(lp);
		return (0);
	}

	// mark object to show that lp object is existant
	put_lpx(env, ret, lp);
	return (ret);
}


JNIEXPORT jboolean JNICALL
Java_org_gnu_glpk_GlpkSolver_writeCpxlp(JNIEnv *env, jobject obj, jstring fname)
{
	jboolean ret;

	const char* chr = (*env)->GetStringUTFChars(env, fname, 0);
	ret = _glp_lpx_write_cpxlp(get_lpx(env, obj), (char*)chr) == 0;
	(*env)->ReleaseStringUTFChars(env, fname, chr);

	return(ret);
}


/*    *********   does this work?? ***********  */
JNIEXPORT jobject JNICALL
Java_org_gnu_glpk_GlpkSolver_readModel(JNIEnv *env, jclass cls,
	jstring model, jstring data, jstring output)
{
	jobject ret;
	jmethodID mid;
	LPX *lp;
	const char *chr_model = 0;
	const char *chr_data = 0;
	const char *chr_output = 0;

	if (model != 0)
		chr_model = (*env)->GetStringUTFChars(env, model, 0);
	if (data != 0)
		chr_data = (*env)->GetStringUTFChars(env, data, 0);
	if (output != 0)
		chr_output = (*env)->GetStringUTFChars(env, output, 0);

	lp = _glp_lpx_read_model((char*)chr_model, (char*)chr_data, (char*)chr_output);

	if (model != 0)
		(*env)->ReleaseStringUTFChars(env, model, chr_model);
	if (data != 0)
		(*env)->ReleaseStringUTFChars(env, data, chr_data);
	if (output != 0)
		(*env)->ReleaseStringUTFChars(env, output, chr_output);

	if (lp == 0)
	return (0);

	mid = (*env)->GetMethodID(env, cls, "<init>", "()V");
	if (mid == 0)
	{
		_glp_lpx_delete_prob(lp);
		return (0);
	}
	ret = (*env)->NewObject(env, cls, mid);
	if ((*env)->ExceptionOccurred(env))
	{
		_glp_lpx_delete_prob(lp);
		return (0);
	}

	put_lpx(env, ret, lp);
	return (ret);
}


JNIEXPORT jboolean JNICALL
Java_org_gnu_glpk_GlpkSolver_printProb(JNIEnv *env, jobject obj,
									   jstring fname)
{
	jboolean ret;

	const char* chr = (*env)->GetStringUTFChars(env, fname, 0);
	ret = _glp_lpx_print_prob(get_lpx(env, obj), (char*)chr) == 0;
	(*env)->ReleaseStringUTFChars(env, fname, chr);

	return (ret);
}


JNIEXPORT jboolean JNICALL
Java_org_gnu_glpk_GlpkSolver_printSol(JNIEnv *env, jobject obj, jstring fname)
{
	jboolean ret;

	const char* chr = (*env)->GetStringUTFChars(env, fname, 0);
	ret = _glp_lpx_print_sol(get_lpx(env, obj), (char*)chr) == 0;
	(*env)->ReleaseStringUTFChars(env, fname, chr);

	return (ret);
}


JNIEXPORT jboolean JNICALL
Java_org_gnu_glpk_GlpkSolver_printSensBnds(JNIEnv *env, jobject obj,
										   jstring fname)
{
	jboolean ret;

	const char* chr = (*env)->GetStringUTFChars(env, fname, 0);
	ret = _glp_lpx_print_sens_bnds(get_lpx(env, obj), (char*)chr) == 0;
	(*env)->ReleaseStringUTFChars(env, fname, chr);

	return (ret);
}


JNIEXPORT jboolean JNICALL
Java_org_gnu_glpk_GlpkSolver_printIps(JNIEnv *env, jobject obj, jstring fname)
{
	jboolean ret;

	const char* chr = (*env)->GetStringUTFChars(env, fname, 0);
	ret = _glp_lpx_print_ips(get_lpx(env, obj), (char*)chr) == 0;
	(*env)->ReleaseStringUTFChars(env, fname, chr);

	return (ret);
}


JNIEXPORT jboolean JNICALL
Java_org_gnu_glpk_GlpkSolver_printMip(JNIEnv *env, jobject obj, jstring fname)
{
	jboolean ret;

	const char* chr = (*env)->GetStringUTFChars(env, fname, 0);
	ret = _glp_lpx_print_mip(get_lpx(env, obj), (char*)chr) == 0;
	(*env)->ReleaseStringUTFChars(env, fname, chr);

	return (ret);
}

