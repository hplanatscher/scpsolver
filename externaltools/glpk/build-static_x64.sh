# Build file for a static GLPK-library on a linux 32-bit machine
# Please read the instructions of the INSTALL file first
# Author: Michael Schober
# Date: 10/07/21

#cd lib
# Compiling files
echo $JAVA_HOME
gcc -fPIC -c -I$JAVA_HOME/include/ -I$JAVA_HOME/include/linux/ -Ilib/ux_64 lib/ux_64/lpx.c
gcc -fPIC -c -I$JAVA_HOME/include/ -I$JAVA_HOME/include/linux/ -I/usr/local/include/ -Ilib/ux_64/ lib/ux_64/glpk_jni.c
gcc -fPIC --shared -W -I$JAVA_HOME/include/ -I$JAVA_HOME/include/linux/ -L/usr/local/lib/ glpk_jni.o lpx.o lib/ux_64/libglpk.a lib/ux_64/libltdl.a lib/ux_64/libz.a -lm -ldl -o libglpkjni_x64.so
# Cleaning up
mv libglpkjni_x64.so build/
rm glpk_jni.o lpx.o
