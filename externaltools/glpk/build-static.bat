rem Builds the glpkjni.dll on a windows machine
rem ===========================================
rem Author: Michael Schober Date: 07/30/2010
rem We need the correct paths! Please check the
rem following paths, before running the script.

rem Adding MinGW to the path variable ...
set PATH=%PATH%;C:\MinGW\bin

rem Setting path to Java SDK
set JAVASDK=C:\Programme\Java\jdk1.6.0_19

rem Compiling ...
gcc -fPIC -c -I%JAVASDK%\include -I%JAVASDK%\include\win32 -Ilib\win_32 lib\glpk_jni.c
gcc -fPIC --shared -W1 -Wl,--kill-at -I%JAVASDK%\include\ -I%JAVASDK%\include\win32\ glpk_jni.o lib\win_32\libglpk.a lib\win_32\libltdl.dll.a -lm lib\win_32\libz.a C:\WINDOWS\system32\kernel32.dll -o glpkjni.dll

rem Cleaning up ...
del glpk_jni.o
move glpkjni.dll build