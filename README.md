# SCPSolver - an easy to use Java Linear Programming Interface

Authors: Hannes Planatscher and Michael Schober. 
Thanks: SCPSolver was developed using Eclipse and optimized using JProfiler. 

SCPSolver should enable a Java developer to use Linear Programming in 5 minutes. 

3 steps to start (works for Windows, Linux and Mac)
Download the SCPSolver.jar and a solver pack:

GLPK Solver Pack: GLPKSolverPack.jar (please note the GPL licensing terms)
LPSOLVE Solver Pack: LPSOLVESolverPack.jar (please note the LGPL licensing terms)
Add both jar-files to your Eclipse project (or the classpath)

That is it. SCPSolver will take care of extracting the required libraries, so you do not have to.

Please find more information on http://scpsolver.org.
	
