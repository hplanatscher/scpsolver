package test;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for scpsolver.test");
		//$JUnit-BEGIN$
		suite.addTestSuite(UtilsTest.class);
		suite.addTestSuite(LinearProgramTest.class);
		suite.addTestSuite(SmallLinearProgramTest.class);
		suite.addTestSuite(QuadraticProgramTest.class);
		//$JUnit-END$
		return suite;
	}

}
